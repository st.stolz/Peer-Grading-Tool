var colorCompetences = "#1b5e20";
var colorCompetencesText = "#ffffff";
var colorUsers = "#81c784";
var colorWeighting = "#e8f5e9";
var logoText = 'Peer-Grading Tool';
var pointInfo = '0 → hardly noticeable work performance\n'+
  '1 → results were partially achieved\n'+
  '2 → expected results were achieved\n'+
  '3 → outstanding performance (comment your rating)';
var competences = ["Presentation", "Professional Content", "Appearance", "Pronunciation", "Handout"];
var weights = [1,2,1,1,2];
var data = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'.split('');
var userFirstSur = "Firstname Surname";
var userTemplate = "user_{0}@gmail.com";

function createTemplate(sheet, nUsers, nCompetences) {
  
  settings = getSettings();
  
  var row;
  
  nUsers = parseInt(nUsers);
  nCompetences = parseInt(nCompetences);

  competences.length = nCompetences;
  weights.length = nCompetences;
  
  /**
   * Competences Headers
   */
  row = 2;
  sheet.getRange(row,1).setBorder(true, false, false, false, false, false).setBackground(colorUsers);
  sheet.getRange(row,2).setValue(settings['userText']).setBorder(true, false, true, true, false, false).setBackground(colorUsers).setFontWeight("bold");
  for (var i = 0; i < competences.length; i++) {
    sheet.getRange(row,3+i).setValue(competences[i]).setBackground(colorCompetences).setBorder(null, null, true, null, false, false).setFontColor(colorCompetencesText);    
    sheet.autoResizeColumn(3+i);
  }
  sheet.getRange(row,competences.length+3).setValue(settings['sumText']).setBorder(true, true, false, true, false, false).setBackground(colorUsers).setFontWeight("bold");
  
  /**
   * Weights Headers
   */
  row = 3;
  sheet.getRange(row,1).setBorder(false, false, false, false, false, false).setBackground(colorUsers);
  sheet.getRange(row,2).setValue(settings['weightingText']).setBorder(true, true, true, false, false, false).setBackground(colorWeighting).setFontWeight("bold");
  for (var i = 0; i < weights.length; i++) {
    var value = weights[i];
    if(!value){
      value = 1;
    }
    sheet.getRange(row,3+i).setValue(value).setBackground(colorWeighting).setBorder(null, null, true, null, false, false);    
  }
  sheet.getRange(row,competences.length+3).setValue("Average Points").setBorder(false, true, false, true, false, false).setBackground(colorUsers).setFontWeight("bold");
  
  /**
   * User Ratings
   */
  row = 4;
  for (var i = 0; i < nUsers; i++) {
    sheet.getRange(row+i,1).setValue(userFirstSur).setBackground(colorUsers).setBorder(null, null, null, null, false, false);   
    sheet.getRange(row+i,2).setValue(userTemplate.replace('{0}',data[i])).setBackground(colorUsers).setBorder(null, null, null, true, false, false);  
    range = columnToLetter(3)+(row+i)+":"+columnToLetter(competences.length+2)+(row+i);
    sheet.getRange(row+i,competences.length+3).setValue("=IF(COUNT("+range+") > 0; SUM("+range+")/COUNT("+range+'); "")').setBackground(colorUsers).setBorder(null, true, null, true, false, false);    
  }
  
  sheet.autoResizeColumn(1);
  sheet.autoResizeColumn(2);
  
  /**
   * First Row
   */
  row = 1;
  sheet.getRange(row,1,1,2).merge().setValue(pointInfo).setBackground(colorCompetences).setBorder(true, true, true, false, false, false).setFontWeight("bold").setFontColor(colorCompetencesText);  
  sheet.getRange(row,3).setValue(settings['competencesText']).setBackground(colorCompetences).setBorder(true, false, false, true, false, false).setFontWeight("bold").setFontColor(colorCompetencesText);
  sheet.autoResizeColumn(3);
  sheet.getRange(row,4,1,competences.length).setValue(logoText).setFontSize(24).merge().setBorder(false, true, true, false, false, false).setHorizontalAlignment('right');
  
  /**
   * Last Row
   */
  row = 4 + nUsers;
  sheet.getRange(row,1,1,2).setValue('').setBorder(false, true, true, false, false, false).setBackground(colorUsers);  
  for (var i = 0; i < competences.length; i++) {
    sheet.getRange(row,i+3).setValue('').setBorder(true, false, true, false, false, false).setBackground(colorUsers);    
  }
  sheet.getRange(row,3+competences.length).setValue('').setBorder(false, false, true, true, false, false).setBackground(colorUsers);  
  
  return sheet;
}

function columnToLetter(column)
{
  var temp, letter = '';
  while (column > 0)
  {
    temp = (column - 1) % 26;
    letter = String.fromCharCode(temp + 65) + letter;
    column = (column - temp - 1) / 26;
  }
  return letter;
}